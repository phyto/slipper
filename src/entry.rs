#[derive(Clone)]
pub struct Entry {
	title: String,
	url: String,
	score: usize,
}

//use select::predicate::{Attr, Class, Name, Predicate};
use select::predicate::*;

impl Entry {
	pub fn from_lobsters(node: &select::node::Node) -> Result<Self, Box<dyn std::error::Error>> {
		Ok(Self {
			title: node
				.find(Class("u-url"))
				.next()
				.ok_or("Couldn't find title")?
				.inner_html(),
			url: node
				.find(Class("u-url"))
				.next()
				.ok_or("couldn't find url")?
				.attr("href")
				.ok_or("invalid url href")?
				.to_owned(),
			score: node
				.find(Class("score"))
				.next()
				.ok_or("couldn't find score")?
				.inner_html()
				.parse::<usize>()?,
		})
	}
}

impl Entry {
	pub fn summary(&self) -> String {
		format!("{:<3} {}", self.score, self.title)
	}
	pub fn title(&self) -> &str {
		&self.title
	}

	pub fn url(&self) -> &str {
		&self.url
	}

	pub fn score(&self) -> usize {
		self.score
	}
}

pub fn parse_frontpage_entries(html_root: &str) -> Result<Vec<Entry>, Box<dyn std::error::Error>> {
	use select::document::Document;
	use select::predicate::{Attr, Class, Name, Predicate};

	let document = Document::from(html_root);

	let entries = document
		.find(Class("story"))
		.map(|n| Entry::from_lobsters(&n).unwrap())
		.collect::<Vec<Entry>>();

	Ok(entries)
}
