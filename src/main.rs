#![allow(unused_imports)]

use select::document::Document;
use select::predicate::{Attr, Class, Name, Predicate};

mod entry;
use entry::Entry;

use cursive::event::EventResult;
use cursive::traits::*;
use cursive::views::{Button, Dialog, DummyView, LinearLayout, OnEventView, SelectView, TextView};
use cursive::Cursive;

pub fn main() -> Result<(), Box<dyn std::error::Error>> {
	let mut siv = cursive::default();
	{
		use cursive::theme::*;
		siv.set_theme(Theme {
			shadow: false,
			borders: BorderStyle::Outset,
			palette: Palette::terminal_default(),
		});
	}

	let postlist = SelectView::<Entry>::new()
		.on_submit(open_url)
		.with_name("postlist")
		.full_screen();

	let buttons = LinearLayout::vertical()
		//.child(Button::new("Open URL", open_url))
		//.child(Button::new("Delete", delete_name))
		.child(DummyView)
		.child(Button::new("Quit", Cursive::quit));

	siv.add_layer(
		Dialog::around(
			LinearLayout::horizontal()
				.child(postlist)
				.child(DummyView)
				.child(buttons),
		)
		.title("Lobste.rs posts"),
	);

	let html_root: String;

	if !cfg!(nocache) {
		html_root = include_str!("index.html").to_owned();
	} else {
		let client = reqwest::blocking::Client::builder()
			.user_agent("slipper")
			.build()?;
		let req = client.get("https://lobste.rs/").send()?;
		dbg!(&req);
		html_root = req.text()?;
		dbg!(&html_root);
	}

	for e in entry::parse_frontpage_entries(&html_root)? {
		siv.call_on_name("postlist", |view: &mut SelectView<Entry>| {
			view.add_item(e.clone().summary(), e);
		});
	}

	siv.run();

	Ok(())
}

fn open_url(s: &mut Cursive, entry: &Entry) {
	match open::that(entry.url()) {
		Ok(()) => (),
		Err(e) => s.add_layer(Dialog::info(format!("Error opening URL!\n{e}"))),
	}
}
